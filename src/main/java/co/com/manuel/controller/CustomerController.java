package co.com.manuel.controller;

import java.util.Optional;

import javax.ws.rs.Consumes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.manuel.persistence.entity.Customer;
import co.com.manuel.persistence.repository.CustomerRepository;


@Consumes("application/json")
@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
        
	@Autowired
	private CustomerRepository customerRepository;
     
    @GetMapping()
    public @ResponseBody Optional<Customer> getCustomerById
    (
		@RequestParam(required = true, name = "id") int id	
    ) 
    {                        
        return customerRepository.findById(id);        
    }  
}
