package co.com.manuel.controller.core.validator;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import co.com.manuel.controller.request.OrderRequest;

@Component
public class OrderRequestValidator implements Validator{
	
	 Logger logger = LoggerFactory.getLogger(OrderRequestValidator.class);
	
	 @Autowired
	 private MessageSource messageSource;

	@Override
	public boolean supports(Class<?> clazz) {
		return OrderRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		logger.trace("validate order request");
		OrderRequest orderRequest = (OrderRequest) target;        
		
		if(orderRequest.getCustomer_id() <= 0) {
			errors.rejectValue("products", messageSource.getMessage("order.customer_id.null.error", null, Locale.getDefault()),
                    "customer id must not be null and not be equal or less than 0");
		}	
		
		if(StringUtils.isEmpty(orderRequest.getDeliveryAddress()) ) {
			errors.rejectValue("products", messageSource.getMessage("order.delivery_adrress.empty.error", null, Locale.getDefault()),
                    "delivery address must not be empty or null");
		}
		
		if(!StringUtils.isEmpty(orderRequest.getDeliveryAddress()) && (orderRequest.getDeliveryAddress().length() > 191 )) {
			errors.rejectValue("products", messageSource.getMessage("order.delivery_adrress.size.error", null, Locale.getDefault()),
                    "the size of delivery address must be equal or less than 191");
		}
        
		
        if (null == orderRequest.getProducts()) {
            errors.rejectValue("products", messageSource.getMessage("order.products.null.error", null, Locale.getDefault()),
                    "products must not be null");
        }
        
        if (null != orderRequest.getProducts()) {
        	orderRequest.getProducts().forEach(p -> {
        		if (!(p instanceof Integer)) {
        			errors.rejectValue("products", messageSource.getMessage("order.products.datatype.error", null, Locale.getDefault()),
        					"data type must be integer");
        		}else if(p instanceof Integer&& p < 1){
        			errors.rejectValue("products", messageSource.getMessage("order.products.positive.error", null, Locale.getDefault()),
                            "order products must be greater than 0");
        		}
        	}); 
	        if(orderRequest.getProducts().size() > 5){
				errors.rejectValue("products", messageSource.getMessage("order.products.size.error", null, Locale.getDefault()),
	                    "the size of products must be equal or less than 5");
			}
	        if(orderRequest.getProducts().isEmpty()){
				errors.rejectValue("products", messageSource.getMessage("order.products.empty.error", null, Locale.getDefault()),
	                    "products must not be empty");
			}
        }
	}

}
