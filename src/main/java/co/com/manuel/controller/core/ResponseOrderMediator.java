package co.com.manuel.controller.core;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import co.com.manuel.controller.response.OrderResponse;
import co.com.manuel.persistence.entity.Order;


public class ResponseOrderMediator {

	private OrderResponse orderResponse;	
	
	public List<OrderResponse> mediate(Iterable<Order> order) {
		
		List<OrderResponse> or= new ArrayList<OrderResponse>();
		
		order.forEach(o -> {			
			orderResponse = new OrderResponse();
			orderResponse.setCreationDate(o.getCreationDate());
			orderResponse.setDeliveryAddress(o.getDeliveryAddress());
			orderResponse.setOrderId(o.getOrderId());
			orderResponse.setTotal(o.getTotal());
			
			List<String> products = o.getOrderDetails().stream().filter(od -> null != od.getProduct()).map(od -> "".concat(String.valueOf(od.getQuantity()).concat(" x ").concat(od.getProduct().getName()))).collect(Collectors.toList());
			orderResponse.setProducts(products);
			or.add(orderResponse);
		});													
		
		return or;
	}
	
}
