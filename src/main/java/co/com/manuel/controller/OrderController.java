package co.com.manuel.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.ws.rs.Consumes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.manuel.controller.core.ResponseOrderMediator;
import co.com.manuel.controller.core.validator.OrderRequestValidator;
import co.com.manuel.controller.request.OrderRequest;
import co.com.manuel.controller.response.OrderResponse;
import co.com.manuel.persistence.entity.Customer;
import co.com.manuel.persistence.entity.Order;
import co.com.manuel.persistence.entity.OrderDetail;
import co.com.manuel.persistence.entity.Product;
import co.com.manuel.persistence.repository.CustomerRepository;
import co.com.manuel.persistence.repository.OrderDetailsRepository;
import co.com.manuel.persistence.repository.OrderRepository;


@Consumes("application/json")
@RestController
@RequestMapping("/api/v1/order")
public class OrderController {
	
	Logger logger = LoggerFactory.getLogger(OrderController.class);
        
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private OrderDetailsRepository orderDetailsRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	

    @Autowired
    private OrderRequestValidator orderRequestValidator;
 
	@InitBinder(value = "orderRequest")
    void initOrderRequestValidator(WebDataBinder binder) {
        binder.setValidator(orderRequestValidator);
    }
	
    @PutMapping("/create")
    public ResponseEntity<Object> createOrder
    (
        @Valid @RequestBody OrderRequest orderRequest
    ) 
    {                   
    	logger.debug("incoming request on resources /create");
    	logger.trace("order request: {}", orderRequest.toString());
    	
    	logger.trace("query customer ...");
    	Optional<Customer> oc = customerRepository.findById(orderRequest.getCustomer_id());
    	logger.trace("query customer ok");
    	
    	
    	logger.trace("filter products ...");
    	Customer c = null;    	
    	List<Product> listProducts = new ArrayList<>();
    	if(oc.isPresent()) {
    		c = oc.get();
    		if(!c.getProducts().isEmpty()) {
    			for(Integer i : orderRequest.getProducts()) {
    				if(c.getProducts().stream().filter(p -> p.getProductId() == i).count() < 1) {    					 
    					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
    							.body("product " + i + " cannot be ordered by customer");
    				}else {   
    					logger.trace("product " + i + " found");
    					listProducts.addAll(c.getProducts().stream().filter(p -> p.getProductId() == i).collect(Collectors.toList()));
    				}
    			}    			
    		}
    	}else {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("client could not be found");
    	}
    	logger.trace("filter products ok");
    	
    	logger.trace("build order detail ...");
    	double total = 0;
    	// ConcurrentHashMap should be use for avoid concurrency conflict
    	Map<Integer, OrderDetail> orderDetailMap = new ConcurrentHashMap<Integer, OrderDetail>();    	    	
    	for(Product lp: listProducts){    		
    		total += lp.getPrice();    		    		    		    		    		    	    		    		    		    		    		   
    		if(orderDetailMap.containsKey(lp.getProductId())) {    		    			
    			OrderDetail odToModify = orderDetailMap.remove(lp.getProductId());
    			odToModify.setQuantity(odToModify.getQuantity() + 1);    			
    			orderDetailMap.put(lp.getProductId(), odToModify);    			    			    			    			    			
    		}else {
    			OrderDetail od = new OrderDetail();
        		od.setProduct(lp);  
        		od.setProductDescription(lp.getProductDescription());
        		od.setPrice(lp.getPrice());
    			od.setQuantity(1);	    		        		        	
    			orderDetailMap.put(lp.getProductId(), od);
    		}    	
    	}
    	logger.trace("build order detail ok");
    	
    	logger.trace("build order ...");
    	Order order = new Order();    	
    	order.setCustomer(c);
    	order.setCreationDate(Calendar.getInstance(TimeZone.getTimeZone("GMT-5:00")).getTime());
    	order.setDeliveryAddress(orderRequest.getDeliveryAddress());    
    	order.setTotal(total);
    	logger.trace("build order ok");
    	
    	logger.trace("save order ...");
    	order = orderRepository.save(order);
    	logger.trace("save order ok {}", order.toString());
    	  
    	logger.trace("update order detail with order id ...");
    	for (Entry<Integer, OrderDetail> set : orderDetailMap.entrySet()) { 
    		OrderDetail odToModify = orderDetailMap.remove(set.getKey());
			odToModify.setOrder(order);
			odToModify.setOrderId(order.getOrderId());
			orderDetailMap.put(set.getKey(), odToModify);
       }
        logger.trace("update order detail with order id ok");

        logger.trace("save order detail ...");
    	List<OrderDetail> odList = (List<OrderDetail>) orderDetailsRepository.saveAll(new ArrayList<>(orderDetailMap.values()));       
    	order.setOrderDetails(odList);
    	logger.trace("save order detail ok");
    	    	
    	return new ResponseEntity<>(order, HttpStatus.CREATED);    
    }      
       
    @GetMapping("/list")
    public @ResponseBody List<OrderResponse> getOrderByCustomerAndRangeOfDate
    (
		@RequestParam(required = true, name = "customer") int customer,
		@RequestParam(required = true, name = "start") @DateTimeFormat(pattern="dd/MM/yyyy") Date startDate,
		@RequestParam(required = true, name = "end") @DateTimeFormat(pattern="dd/MM/yyyy") Date endDate
    ) 
    {   
    	logger.trace("incoming request on resources /list customer={}, startDate={}, endDate={}", customer, startDate, endDate);
    	
    	logger.debug("setting the end of the day on variable endDate {} ...", endDate);
    	Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT-5:00"));
    	c.setTime(endDate);
    	c.set(Calendar.HOUR_OF_DAY, 23);
    	c.set(Calendar.SECOND, 59);
    	c.set(Calendar.MINUTE, 59);
    	c.set(Calendar.MILLISECOND, 999);
    	endDate = c.getTime();
    	logger.debug("setting the end of the day on variable endDate {} ok", endDate);
    	
    	logger.debug("query order ...");
    	Iterable<Order> orderIterable = orderRepository.getOrdersByCustomerAndRangeOfDate(customer, startDate, endDate);
    	logger.debug("query order ok");
 
    	ResponseOrderMediator mediator = new ResponseOrderMediator();
    	return mediator.mediate(orderIterable);    	    	      
    }    
    
    @GetMapping()
    public @ResponseBody Optional<Order> getOrderById
    (
		@RequestParam(required = true, name = "id") int id	
    ) 
    {                        
        return orderRepository.findById(id);        
    }    
}
