package co.com.manuel.controller.response;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderResponse {
	
	@JsonProperty("Order ID")
    private int orderId;  
	
	@JsonProperty("Creation Date") 
    private Date creationDate;
	
	@JsonProperty("Delivery Address")
    private String deliveryAddress;
	
	@JsonProperty("Total $")
    private double total;
	
	@JsonProperty("Products")
	private List<String> products;

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public List<String> getProducts() {
		return products;
	}

	public void setProducts(List<String> products) {
		this.products = products;
	}
	
	
}
