package co.com.manuel.controller.request;

import java.util.List;

public class OrderRequest {
	
	private int customer_id;
		
	private String deliveryAddress;
			
	private List<Integer> products;
	
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public List<Integer> getProducts() {
		return products;
	}
	public void setProducts(List<Integer> products) {
		this.products = products;
	}
	@Override
	public String toString() {
		return "OrderRequest [customer_id=" + customer_id + ", deliveryAddress=" + deliveryAddress + ", products="
				+ products + "]";
	}	
	
	

}
