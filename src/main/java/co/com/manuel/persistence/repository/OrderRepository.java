package co.com.manuel.persistence.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import co.com.manuel.persistence.entity.Order;

public interface OrderRepository extends CrudRepository<Order, Integer> {     			

	@Modifying
	@Query("SELECT o FROM Order o WHERE o.customer.customer_id = :customer_id and o.creationDate BETWEEN :startDate AND :endDate")
	Iterable<Order> getOrdersByCustomerAndRangeOfDate(@Param("customer_id") int customer_id, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
