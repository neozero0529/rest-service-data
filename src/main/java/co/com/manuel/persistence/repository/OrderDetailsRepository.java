package co.com.manuel.persistence.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.manuel.persistence.entity.OrderDetail;

public interface OrderDetailsRepository extends CrudRepository<OrderDetail, Integer> {
     			

	
}
