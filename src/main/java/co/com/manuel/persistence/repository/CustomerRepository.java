package co.com.manuel.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import co.com.manuel.persistence.entity.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

}
