package co.com.manuel.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


@Entity
@NamedQuery(name="Product.findAll", query="SELECT p FROM Product p")
public class Product implements Serializable {
	private static final long serialVersionUID = 4997116055073126039L;

	@Id
	@Column(name="product_id")
	private int productId;

	private String name;

	private double price;

	@Column(name="product_description")
	private String productDescription;

//	//bi-directional many-to-many association to Customer
//	@ManyToMany(mappedBy="products", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	private List<Customer> customers;

//	//bi-directional many-to-one association to OrderDetail
//	@OneToMany(mappedBy="product", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	private List<OrderDetail> orderDetails;

	public Product() {
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getProductDescription() {
		return this.productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

//	public List<Customer> getCustomers() {
//		return this.customers;
//	}
//
//	public void setCustomers(List<Customer> customers) {
//		this.customers = customers;
//	}
//
//	public List<OrderDetail> getOrderDetails() {
//		return this.orderDetails;
//	}
//
//	public void setOrderDetails(List<OrderDetail> orderDetails) {
//		this.orderDetails = orderDetails;
//	}
//
//	public OrderDetail addOrderDetail(OrderDetail orderDetail) {
//		getOrderDetails().add(orderDetail);
//		orderDetail.setProduct(this);
//
//		return orderDetail;
//	}
//
//	public OrderDetail removeOrderDetail(OrderDetail orderDetail) {
//		getOrderDetails().remove(orderDetail);
//		orderDetail.setProduct(null);
//
//		return orderDetail;
//	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", name=" + name + ", price=" + price + ", productDescription="
				+ productDescription + "]";
	}
	
	

}